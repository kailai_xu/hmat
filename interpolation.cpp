//
// Created by kailai on 10/17/17.
//

#include "Cluster.h"
#define PI 3.1415926

double cheb(int n, double x){
    return cos(n*acos(x));
}



double gauss1d(double *x, double *w, double a, double b){
    int i=0,j=0;
    x[i++] = 0.42556283; w[j++] =  0.14776211;
    x[i++] = 0.57443717; w[j++] =  0.14776211;
    x[i++] = 0.2833023 ; w[j++] =  0.13463336;
    x[i++] = 0.7166977 ; w[j++] =  0.13463336;
    x[i++] = 0.16029522; w[j++] =  0.10954318;
    x[i++] = 0.83970478; w[j++] =  0.10954318;
    x[i++] = 0.06746832; w[j++] =  0.07472567;
    x[i++] = 0.93253168; w[j++] =  0.07472567;
    x[i++] = 0.01304674; w[j++] =  0.03333567;
    x[i++] = 0.98695326; w[j++] =  0.03333567;
    for(i=0;i<10;i++){
        x[i] = a + x[i]*(b-a);
    }
}

double gauss1d2(double *x, double *w, double a, double b){
    int i=0,j=0;
    x[i++] = 0.5       ; w[j++] =  0.16511968;
    x[i++] = 0.08198445; w[j++] =  0.09032408;
    x[i++] = 0.91801555; w[j++] =  0.09032408;
    x[i++] = 0.01591988; w[j++] =  0.04063719;
    x[i++] = 0.98408012; w[j++] =  0.04063719;
    x[i++] = 0.33787329; w[j++] =  0.15617354;
    x[i++] = 0.66212671; w[j++] =  0.15617354;
    x[i++] = 0.19331428; w[j++] =  0.13030535;
    x[i++] = 0.80668572; w[j++] =  0.13030535;
    for(i=0;i<9;i++){
        x[i] = a + x[i]*(b-a);
    }
}

double gaussQ(double *x, double *w, double a, double b, int n){
    double h = (b-a)/double(n);
    for(int i=0;i<n;i++){
        double l = a + h*i, r = a + h*(i+1);
        gauss1d(x+i*10,w+i*10,l,r);
    }
    for(int i=0;i<n*10;i++){
        w[i] = w[i]/double(n);
    }
}

//* only accurate if two intervals are far away.
double chebcoeff(double t1, double t2, double s1, double s2,\
            int l, int m, double (*K)(double, double)){
    int n = 100;
    double s = 0;
    //! this is Chebyshev-Gauss quadrature
    //! referece: https://en.wikipedia.org/wiki/Chebyshev%E2%80%93Gauss_quadrature
    for(int i=0;i<n;i++){
        double theta1 = cos((2*i+1)/(2.0*n)*PI);
        for(int j=0;j<n;j++) {
            double theta2 = cos((2*j+1)/(2.0*n)*PI);
            double xp = (t1+t2)/2.0+(t2-t1)/2.0*theta1,
                yp = (s1+s2)/2.0+(s2-s1)/2.0*theta2;
            s += K(xp,yp) *cheb(l, theta1)*cheb(m, theta2)* PI/double(n)*PI/double(n);
        }
//        s += 2*w[i]/sqrt(1-theta1[i]*theta1[i]);
    }
    if(l==0 && m==0)
        return s/(PI*PI);
    else if(l==0 || m==0)
        return s/(PI*PI)*2;
    else
        return s/(PI*PI)*4;
}

double kernel(double x, double y){
    return -1.0/(2*PI)*log(2.0)-1.0/(2*PI)*log(fabs(sin((x-y)/2.0)));
}

double dkernel(double x, double y){
    return -1.0/(4*PI);
}

double eval_dkernel(double t1, double t2, double s1, double s2){
    return -1.0/(4*PI)*(s2-s1);
}


double eval_kernel1(double t1, double t2, double s1, double s2){
    double mid = (t1+t2)/2.0;

    double s = 0;
    if(mid <= s2 && mid >= s1){
//        TODO: quad for log
        double x1[100],w1[100];
        gaussQ(x1,w1,0,(s2-s1)/2.0,10);
        s = log(4.0)*(s2-s1)/2.0;
        for(int i=0;i<100;i++){
            s+=(s2-s1)*log(sin(x1[i]/2.0))*w1[i];
        }
        s = -s/(2*PI);
    }
    else{
        double x1[10],w1[10];
        gauss1d(x1,w1,s1,s2);
        for(int i=0;i<10;i++){
            s += kernel(mid, x1[i])*w1[i]*(s2-s1);
        }
    }
    return s;

}

void fill_matrix(Param *param){
    for(auto b: param->Lp){
//        cout << "writing b" << endl;
        auto t =  b->t;
        auto s = b->s;
        int N = min(param->N, min ( t->num_intervals, s->num_intervals));
//        int N = 5;

        MatrixXd W(t->num_intervals, N);
        double t2 = param->ivec[t->end]->e;
        double t1 = param->ivec[t->start]->s;

        for(int i=t->start;i<=t->end;i++){
            double xi = param->ivec[i]->mid;
            for(int l=0;l<N;l++){
                W(i-t->start,l) = cheb(l, (xi-(t2+t1)/2.0)/((t2-t1)/2.0));
            }
        }
        t->Il = W;

        W.resize(s->num_intervals, N);
        W.setZero();
        double x[10],w[10];
        double s1 = param->ivec[s->start]->s;
        double s2 = param->ivec[s->end]->e;


        for(int i=s->start;i<=s->end;i++){
            gauss1d(x,w,param->ivec[i]->s, param->ivec[i]->e);
            for(int m=0;m<N;m++){
                for(int j=0;j<10;j++){
                    W(i-s->start,m) += cheb(m, (x[j]-(s2+s1)/2.0)/((s2-s1)/2.0))*w[j]*param->ivec[i]->len;
                }

//                W(i-s->start,m) += cheb(m, (param->ivec[i]->mid-(s2+s1)/2.0)/((s2-s1)/2.0));
            }
        }

        s->Ibt = W.transpose();

        W.resize(N,N);
        for(int l=0;l<N;l++){
            for(int m=0;m<N;m++){
                W(l,m) = chebcoeff(param->ivec[t->start]->s, param->ivec[t->end]->e,
                                   param->ivec[s->start]->s, param->ivec[s->end]->e,
                        l,m,kernel);
            }
        }
        b->C = W;

#if 0
        W.resize(t->num_intervals,s->num_intervals);
        W.setConstant(0);
        for(int i=t->start;i<=t->end;i++) {
            for (int j = s->start; j <= s->end; j++) {
                W(i-t->start, j-s->start) = eval_kernel1(param->ivec[i]->s, param->ivec[i]->e, param->ivec[j]->s,
                                                         param->ivec[j]->e);
//                W(i-t->start, j-s->start) = kernel(param->ivec[i]->mid, param->ivec[j]->mid);
            }
        }
        MatrixXd E = W-t->Il*b->C*s->Ibt;
        cout << "Approximation Error: " << E.norm() << endl;

        if(!E.norm()<1e-6){
            double ss = 0;
            double xi = param->ivec[t->start]->mid;
            double yi = param->ivec[s->start]->mid;
            for(int i=0;i<N;i++)
                for(int j=0;j<N;j++){
                    ss += cheb(i, (xi-(t2+t1)/2.0)/((t2-t1)/2.0)) *
                            chebcoeff(param->ivec[t->start]->s, param->ivec[t->end]->e,
                                      param->ivec[s->start]->s, param->ivec[s->end]->e,
                                      i,j,kernel)*
                            cheb(j, (yi-(s2+s1)/2.0)/((s2-s1)/2.0));
                }
            cout << ss << ' ' << kernel(xi,yi) << endl;
            cout << W << endl;
            cout << endl;
            cout << t->Il*b->C*s->Ibt << endl;
            cout << endl;
        }
#endif
    }
    for(auto b: param->Ln){
        auto t = b->t;
        auto s = b->s;
        MatrixXd W(t->num_intervals, s->num_intervals);
        W.setConstant(0);
        for(int i=t->start;i<=t->end;i++) {
            for (int j = s->start; j <= s->end; j++) {
                W(i-t->start, j-s->start) = eval_kernel1(param->ivec[i]->s, param->ivec[i]->e, param->ivec[j]->s,
                                                         param->ivec[j]->e);
            }
        }
        b->C = W;
    }
}

int dense = 0;
void dense_matrix(Param *param){
    for(auto b: param->Lp){
        auto t = b->t;
        auto s = b->s;
        MatrixXd W(t->num_intervals, s->num_intervals);
        W.setZero();
//        dense += 1;
        for(int i=t->start;i<=t->end;i++) {
            for (int j = s->start; j <= s->end; j++) {
                W(i-t->start, j-s->start) = eval_kernel1(param->ivec[i]->s, param->ivec[i]->e, param->ivec[j]->s,
                                                         param->ivec[j]->e);
            }
        }

        b->C = W;
    }
    for(auto b: param->Ln){
        auto t = b->t;
        auto s = b->s;
        MatrixXd W(t->num_intervals, s->num_intervals);
        W.setConstant(0);
      for(int i=t->start;i<=t->end;i++) {
        for (int j = s->start; j <= s->end; j++) {
            W(i-t->start, j-s->start) = eval_kernel1(param->ivec[i]->s, param->ivec[i]->e, param->ivec[j]->s,
                                                     param->ivec[j]->e);
        }
        }
        b->C = W;
    }
}




