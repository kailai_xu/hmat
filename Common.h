//
// Created by kailai on 10/18/17.
//

#ifndef HMAT_COMMON_H
#define HMAT_COMMON_H

#include <time.h>
#include <stdio.h>

/**
 * Timer class is used to time the executables.
 * Example
 * =======
 * Timer timer;
 * ...
 * timer.toc_o();
 */
class Timer{
public:
    clock_t t;
    Timer();
    void toc_o();
    double toc();
};


#endif //HMAT_COMMON_H
