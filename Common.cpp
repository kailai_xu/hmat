//
// Created by kailai on 10/18/17.
//

#include "Common.h"


Timer::Timer(){
    t = clock();
}

void Timer::toc_o(){
    t = clock()-t;
    printf("Time elapse: %f seconds\n", ((float)t)/CLOCKS_PER_SEC);
}

double Timer::toc(){
    t = clock()-t;
    return ((float)t)/CLOCKS_PER_SEC;
}
