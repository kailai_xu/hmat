load fillmatrix.txt
I = fillmatrix;
A = sparse(I(:,1),I(:,2),I(:,3));
A = full(A);

load fillmatrix2.txt
I = fillmatrix2;
B = sparse(I(:,1),I(:,2),I(:,3));
B = full(B);

sum(sum(abs(A-B)))/numel(A)