//
// Created by kailai on 10/17/17.
//

#include "interpolation.h"
#include "Cluster.h"

VectorXd matvec(Param *param, VectorXd& vec){
    assert(vec.size()==param->ivec.size());
    VectorXd y(vec.size());
    y.setZero();

    for(auto b: param->Lp){
//        cout << b->t->num_intervals << ' ' << b->s->num_intervals << ' ' << b->C.rows()<<endl;
        y.segment(b->t->start,b->t->num_intervals) += (b->t->Il)*( (b->C) * (( b->s->Ibt )*vec.segment(b->s->start,b->s->num_intervals)) );
    }
    for(auto b: param->Ln){
        y.segment(b->t->start,b->t->num_intervals) += b->C*vec.segment(b->s->start, b->s->num_intervals);
    }
    return y;
}

VectorXd dense_matvec(Param *param, VectorXd& vec){
    assert(vec.size()==param->ivec.size());

    VectorXd y(vec.size());
    y.setZero();
    for(auto b: param->Lp){
        y.segment(b->t->start,b->t->num_intervals) += b->C * vec.segment(b->s->start,b->s->num_intervals);
    }
    for(auto b: param->Ln){
        y.segment(b->t->start,b->t->num_intervals) += b->C * vec.segment(b->s->start,b->s->num_intervals);
    }
    return y;
}
