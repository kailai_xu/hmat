cmake_minimum_required(VERSION 3.8)
project(hmat)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp cheb.cpp Cluster.cpp  interpolation.cpp arithm.cpp Common.cpp Common.h)
add_executable(hmat ${SOURCE_FILES})