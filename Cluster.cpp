//
// Created by kailai on 10/17/17.
//

#include "Cluster.h"
#define PI 3.141592654


Cluster::Cluster(int s, int e) {
    start = s;
    end = e;
    num_intervals = e-s+1;
}

Block::Block(Cluster *left, Cluster *right) {
    t = left;
    s = right;
}

void constructCluster(Cluster* t, Param* param){
    double diam = 0;
    for(int i=0;i<t->num_intervals;i++){
        diam += param->ivec[i]->len;
    }
    if(diam<param->D||t->num_intervals<(param->n*0.2)) {
        param->leaves.insert(t);
        return;
    }

    auto t1 = new Cluster(t->start, (t->start+t->end)/2);
    auto t2 = new Cluster((t->start+t->end)/2+1, t->end);
    t->sons.push_back(t1);
    t->sons.push_back(t2);
    constructCluster(t1, param);
    constructCluster(t2, param);
}

Cluster* circle_construct_cluster(Param *param){
    int n = param->n;
    double h = 2*PI/(double)n;
    for(int i=0;i<n;i++){
        auto interval = new Interval(h*i,h*(i+1));
        param->ivec.push_back(interval);
    }
    auto root = new Cluster(0,n-1);
    constructCluster(root, param);
    return root;
}

bool isAdmissible(Cluster * t, Cluster *s, Param *param){
    double x1 = param->ivec[t->start]->s;
    double x2 = param->ivec[t->end]->e;
    double y1 = param->ivec[s->start]->s;
    double y2 = param->ivec[s->end]->e;
    if(y1<=x1){
        double temp;
        temp = y1;
        y1 = x1;
        x1 = temp;
        temp = y2;
        y2 = x2;
        x2 = temp;
    }
    if(y1<=x2) return false;
    double dist = min(y1-x2, 2*PI-y2+x1);
    double d = min(x2-x1,y2-y1);
    return d<=param->eta*dist;
}

void constructBlock(Block* b, Param *param){
    if(param->blocks.count(make_pair(b->t,b->s))){
        delete b;
        return;
    }
    else{
        param->blocks.insert(make_pair(b->t,b->s));
    }
    if(isAdmissible(b->t, b->s, param)) {
        param->Lp.insert(b);
        return;
    }
    if(param->leaves.count(b->t)&& \
        param->leaves.count(b->s)){
        param->Ln.insert(b);
        return;
    }
    if((!b->s->sons.empty()) && (b->t->sons.empty())){
        auto b1 = new Block(b->t, b->s->sons[0]);
        auto b2 = new Block(b->t, b->s->sons[1]);
        constructBlock(b1, param);
        constructBlock(b2, param);
        return;
    }
    if(!b->t->sons.empty()&& (b->s->sons.empty())){
        auto b1 = new Block(b->t->sons[0],b->s);
        auto b2 = new Block(b->t->sons[1],b->s);
        constructBlock(b1, param);
        constructBlock(b2, param);
        return;
    }
    if((!b->t->sons.empty()) && (!b->s->sons.empty())){
        for(int i=0;i<2;i++)
            for(int j=0;j<2;j++){
                auto b1 = new Block(b->t->sons[i],b->s->sons[j]);
                constructBlock(b1, param);
            }
        return;
    }

    cout << "Should not land here "<<endl;

}