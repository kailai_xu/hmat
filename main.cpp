#include <iostream>
#include "Cluster.h"
#include "interpolation.h"
#include <stdlib.h>
#include <iomanip>
#include <queue>
#include "Common.h"
#include <fstream>
#include <string>
using namespace std;

#define PI 3.1415926
double testfunc1(double x){
    return x*cos(x)+log(x+1.0);
}

//! note: may be inaccurate
void testgauss(){
    double x[10],w[10],x1[9],w1[9];
    double s1=0,s2=0;
    gauss1d(x,w,-1.0,-0.99);
    gauss1d2(x1,w1,-1.0,-0.99);
    for(int i=0;i<10;i++){
        s1 += testfunc1(x[i])*0.01*w[i];
    }
    for(int i=0;i<9;i++){
        s2 += testfunc1(x1[i])*0.01*w1[i];
    }
    cout << s1 << "  " << s2 << endl;
}

void testgaussQ(){
    double x1[10], w1[10];
    gauss1d(x1,w1,1,1.01);
    double s1=0;
    for(int i=0;i<10;i++) s1+= testfunc1(x1[i])*w1[i]*0.01;

    double x[100],w[100];
    gaussQ(x,w,1,1.01,10);
    double s = 0;
    for(int i=0;i<100;i++) s += testfunc1(x[i])*w[i]*0.01;
    cout << setprecision(16) << s1 << ' '<<s << endl;
}

double testkernel(){
    srand(123);
    double theta1 = rand();
    double theta2 = rand();
    double s = sqrt(pow(cos(theta1)-cos(theta2),2)+ pow(sin(theta1)-sin(theta2),2.0));
    s = -1.0/(2*PI)*log(s);
    cout << s-kernel(theta1,theta2) << endl;
}

//! not sure if singular integration works
double test_eval_ln_sin(){
    cout << eval_kernel1(0.4,0.6,0.4,0.6) << endl;
    cout << eval_kernel1(0.4,0.6,0.1,0.2) << endl;
}

double const_func(double x,double y){
    return 1.0;
}
double x_func(double x, double y){
//    return pow(x,2)/sqrt(x*x*y*y+1);
    return kernel(x,y);
}

double test_chebcoeff(){
    int m = 10, n = 10;
    double xmin = 2.0, xmax = 2.1, ymin = 3.8, ymax = 3.9;
    vector<vector<double>> coef;
    coef.resize(m);
    for(int i=0;i<m;i++){
        coef[i].resize(n);
    }
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++){
            coef[i][j] = chebcoeff(xmin, xmax, ymin, ymax,i,j,x_func);
        }
    double x[9];
    double y[9];
    for(int i=0;i<9;i++)
    {
        x[i] = i*(xmax-xmin)/10.0+xmin;
        y[i] = i*(ymax-ymin)/10.0+ymin;
    }
    double exact[9][9];
    double numeric[9][9];
    for(int i=0;i<9;i++)
        for(int j=0;j<9;j++){
            exact[i][j] = x_func(x[i],y[j]);
        }
    double total_err = 0;
    for(int ii=0;ii<9;ii++){
        for(int jj=0;jj<9;jj++){
            double s = 0.0;
            for(int i=0;i<m;i++)
                for(int j=0;j<n;j++){
                    s += coef[i][j]*cheb(i,(x[ii]-(xmin+xmax)/2.0)/((xmax-xmin)/2.0))*
                            cheb(j,(y[jj]-(ymin+ymax)/2.0)/((ymax-ymin)/2.0));
                }
            numeric[ii][jj] = s;
            total_err += pow(numeric[ii][jj]-exact[ii][jj],2);
        }
    }
//    for(int i=0;i<9;i++){
//        for(int j=0;j<9;j++)
//        cout << numeric[i][j] << endl;
//    }
    cout << "x\ty\texact\tnumerical\terror"<<endl;
    for(int i=0;i<9;i++)
        for(int j=0;j<9;j++){
            cout << x[i] << '\t' << y[j] << '\t' << exact[i][j] << '\t' << numeric[i][j]\
                           << '\t' << fabs((numeric[i][j]-exact[i][j])/exact[i][j])<<endl;
//
        }
    cout << "Error "<<sqrt(total_err/(81.0)) << endl;
}


void testcircle_construct_cluster(){
    Param param;
    param.D = 2*PI/20;
    param.eta = 1.0;
    param.n = 20;
    param.N = 10;
    auto root = circle_construct_cluster(&param);
    queue<Cluster *> s;
    s.push(root);
    int k=1;
    while(!s.empty()){
        auto cur = s.front();
        s.pop();
        cout << k <<" :: "<< cur->start << ',' << cur->end <<

                                              '\t' << (param.leaves.count(cur)?"Leaf":"Internal")<<endl;
        k++;
        for(auto t: cur->sons) s.push(t);
    }
    cout << "Number of leaves " <<param.leaves.size() << endl;
    for(int i=0;i<param.n;i++){
        cout << "Interval "<<i<< " (" << param.ivec[i]->s << ',' << param.ivec[i]->e << ")\n";
    }

}

void testBlock(){
    Param param;
    param.D = 2*PI/20;
    param.eta = 1.1;
    param.n = 20;
    param.N = 10;
    auto root = circle_construct_cluster(&param);
    Block * b = new Block(root,root);
    constructBlock(b, &param);
    int total = 0;
    cout << "Admissible blocks" << endl;
    for(auto pb: param.Lp){
        printf("(%d,%d),(%d,%d)\t%dx%d\n", pb->t->start, pb->t->end, pb->s->start, pb->s->end,
                pb->t->num_intervals, pb->s->num_intervals);
        total += pb->t->num_intervals*pb->s->num_intervals;
    }
    cout << "Inadmissible blocks" << endl;
    for(auto pb: param.Ln){
        printf("(%d,%d),(%d,%d)\n", pb->t->start, pb->t->end, pb->s->start, pb->s->end);
        total += pb->t->num_intervals*pb->s->num_intervals;
    }
    cout << total << endl;
}


void testfill_matrix(){
    Param param;
//    param.D = 2*PI/320;
//    param.eta = 0.8;
//    param.n = 3200;
//    param.N = 10;
    param.D = 2*PI/20;
    param.eta = 0.8;
    param.n = 200;
    param.N = 2;

    auto root = circle_construct_cluster(&param);
    Block * b = new Block(root,root);
    constructBlock(b, &param);
    Timer timer;
    dense_matrix(&param);
    timer.toc_o();
    FILE * fp = fopen("../fillmatrix.txt","w");
    for(auto b: param.Lp){
        for(int i=0;i<b->t->num_intervals;i++){
            for(int j=0;j<b->s->num_intervals;j++){
                fprintf(fp,"%d %d %f\n",b->t->start+i+1,b->s->start+j+1,b->C(i,j));
            }
        }
    }
    for(auto b: param.Ln){
        for(int i=0;i<b->t->num_intervals;i++){
            for(int j=0;j<b->s->num_intervals;j++){
                fprintf(fp,"%d %d %f\n",b->t->start+i+1,b->s->start+j+1,b->C(i,j));
            }
        }
    }
    fclose(fp);
}

void testfill_matrix2(){
    Param param;
//    param.D = 2*PI/320;
//    param.eta = 0.8;
//    param.n = 3200;
//    param.N = 2;

    param.D = 2*PI/20;
    param.eta = 0.8;
    param.n = 200;
    param.N = 2;

    auto root = circle_construct_cluster(&param);
    Block * b = new Block(root,root);
    constructBlock(b, &param);
    Timer timer;
    fill_matrix(&param);
    timer.toc_o();
    printf("Total number of Lp: %d, total number of Ln: %d\n", param.Lp.size(), param.Ln.size());
    FILE * fp = fopen("../fillmatrix2.txt","w");
    for(auto b: param.Lp){
//        MatrixXd W = b->C;
        MatrixXd W=b->t->Il*b->C*b->s->Ibt;
        for(int i=0;i<b->t->num_intervals;i++){
            for(int j=0;j<b->s->num_intervals;j++){
                fprintf(fp,"%d %d %f\n",b->t->start+i+1,b->s->start+j+1,W(i,j));
            }
        }
    }
    for(auto b: param.Ln){
        for(int i=0;i<b->t->num_intervals;i++){
            for(int j=0;j<b->s->num_intervals;j++){
                fprintf(fp,"%d %d %f\n",b->t->start+i+1,b->s->start+j+1,b->C(i,j));
            }
        }
    }
    fclose(fp);
}

void compute_matrix_error(int n){
    MatrixXd A = MatrixXd::Zero(n,n);
    MatrixXd B = MatrixXd::Zero(n,n);
    ifstream ifile("../fillmatrix.txt");
    ifstream ifile2("../fillmatrix2.txt");
    int i,j;
    double v;
    for(int k=0;k<n*n;k++)
    {
        ifile >> i >> j >> v;
        A(i-1,j-1) = v;
        ifile2 >> i >> j >> v;
        B(i-1,j-1) = v;
    }

    double s = 0;
    for(i=0;i<n;i++){
        for(j=0;j<n;j++)
        s += fabs(A(i,j)-B(i,j));
    }
    cout << "Error " << s/double(n*n) << endl;
}

void testmatvec(){
    Param param, param1;
    param.D = 2*PI/640;
    param.eta = 2;
    param.n = 3200;
    param.N = 3;
    auto root = circle_construct_cluster(&param);
    Block * b = new Block(root,root);
    constructBlock(b, &param);
    fill_matrix(&param);

    param1.D = 2*PI/640;
    param1.eta = 2;
    param1.n = 3200;
    param1.N = 3;
    auto root1 = circle_construct_cluster(&param1);
    Block * b1 = new Block(root1,root1);
    constructBlock(b1, &param1);
    dense_matrix(&param1);

    VectorXd vec = Eigen::VectorXd::Random(param.n);
    Timer timer1;
    auto v1 = dense_matvec(&param1, vec);
    timer1.toc_o();
    Timer timer2;
    auto v2 = matvec(&param, vec);
    timer2.toc_o();
    cout << (v1-v2).norm()/vec.norm() << endl;

}

int main(char **argv, int argc) {
//    testgauss();
//    testgaussQ();
//    testkernel();
//    test_eval_ln_sin();
//    test_chebcoeff();
//    testgauss();
//    testcircle_construct_cluster();
//    testBlock();
//    testfill_matrix();
//    testfill_matrix2();
//    testmatvec();
    testfill_matrix();
    testfill_matrix2();
    compute_matrix_error(200);
    return 0;

}