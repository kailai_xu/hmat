//
// Created by kailai on 10/17/17.
//

#ifndef HMAT_CLUSTER_H
#define HMAT_CLUSTER_H

#include <vector>
#include <iostream>
#include <eigen3/Eigen/Dense>
#include <utility>
#include <set>
#include <map>
using namespace std;

typedef  Eigen::MatrixXd MatrixXd;
typedef  Eigen::VectorXd VectorXd;
class Cluster;
typedef pair< Cluster*, Cluster *> CPAIR;

/**
 * Interval : [s,e]
 */
class Interval{
public:
    Interval(double s, double e){
        this->s=s;
        this->e=e;
        mid = (s+e)/2.0;
        len = e-s;
    }
    double s, e;
    double mid;
    double len;
};

class Cluster {
public:
    Cluster(int s, int e); //! construct from the indexes of intervals s and e
    int start, end;
    vector<Cluster*> sons; //! every cluster has two or zero sons.
    int num_intervals;
    MatrixXd Il; //! used to store auxiliary data on the left(Il) and bottom(Ib)
                // refer to http://stanford.edu/~kailaix/oneequation/ for H-matrix structure
    MatrixXd Ibt;
};

class Block{
public:
    Block(Cluster *left, Cluster *right);
    Cluster *t, *s; //! the corresponding cluster Block is made of.
    //! store the small red matrix for each block.
    //! For A*B type, Block should be modified to hold two matrix
    MatrixXd C;

};

class Param{
public:
    /**
     * Minimum diameter
     */
    double D;
    /**
     * All the intervals
     */
    vector<Interval*> ivec;
    /**
     * Leaves of tree cluster
     */
    set<Cluster*> leaves;
    /**
     * number of intervals
     */
    int n;
    /**
     * Store all the blocks
     */
    set< CPAIR > blocks;
    /**
     * Store all block leaves
     */
    set< Block* > Lp;
    set< Block* > Ln;
    /**
     * parameter for determining admissible
     */
    double eta;
    /**
     * cutoff
     */
    int N;

};

//! construct the cluster structure for a circle
Cluster* circle_construct_cluster(Param *param);
//! construct the block structure from the cluster structure.
void constructBlock(Block* b, Param *param);


#endif //HMAT_CLUSTER_H
