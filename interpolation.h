#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include "Cluster.h"
//! quadratures
double gauss1d(double *x, double *w, double a, double b);
double gauss1d2(double *x, double *w, double a, double b);
double kernel(double x, double y);
//! evaluate the kernel directly
//! int_{s1}^s2 k(x_i, y)dy
double eval_kernel1(double t1, double t2, double s1, double s2);
//! K(x,y) = sum_{i,j} coef[i][j]*cheb(x)*cheb(y)
double chebcoeff(double t1, double t2, double s1, double s2,\
            int l, int m, double (*K)(double, double));
double cheb(int n, double x);
//! split [a,b] into n pieces and do Gauss quadrature on each piece
double gaussQ(double *x, double *w, double a, double b, int n);
//! fill each block with dense matrix(no compression)
void dense_matrix(Param *param);
//! find Il, Ib, C
void fill_matrix(Param *param);
//! matrix vector multiplication
VectorXd matvec(Param *param, VectorXd& vec);
VectorXd dense_matvec(Param *param, VectorXd& vec);

#endif